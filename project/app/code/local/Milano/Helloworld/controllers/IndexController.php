<?php
/**
 * Milano Helloworld
 */

/**
 * class Milano_Helloworld_Helper_Data
 *
 * Main helper.
 * @author Caterina Milano <caterina.milano@thinkopen.it>
 * @version 0.1.0
 * @packege CMS
 * @license GNU General Public License, version 3
 */
class Milano_Helloworld_IndexController extends
    Mage_Core_Controller_Front_Action
{
    /**
     * indexAction
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
}