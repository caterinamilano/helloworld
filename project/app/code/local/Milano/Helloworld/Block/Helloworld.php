<?php
/**
 * Milano Helloworld
 */

/**
 * class Milano_Helloworld_Helper_Data
 *
 * Hello World Block.
 * @author Caterina Milano <caterina.milano@thinkopen.it>
 * @version 0.1.0
 * @packege CMS
 * @license GNU General Public License, version 3
 */
class Milano_Helloworld_Block_Helloworld extends Mage_Core_Block_Template
{
    /**
     * isEnabled
     *
     * Returns true if the module is enabled to be displayed.
     * @return boolean
     */
    public function isEnabled()
    {
        return Mage::helper('milano_helloworld')->isEnabled();
    }

    /**
     * getMessage
     *
     * Returns the custom message, if the module is enabled.
     * @return string|boolean
     */
    public function getMessage()
    {
        if ($this->isEnabled()){
            return Mage::helper('milano_helloworld')->getConfigData
            ('configuration/custom_message');
        }
        return false;
    }
    /**
     * getUrl
     *
     * Returns the Hello World URL
     * @return string
     */
    public function getHelloworldUrl()
    {
        return Mage::getBaseUrl() . 'helloworld';
    }
}
