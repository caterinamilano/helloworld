<?php
/**
 * Milano test
 */

/**
 * class Milano_Test_Helper_Data
 *
 * Main helper.
 * @author Caterina Milano <caterina.milano@thinkopen.it>
 * @version 0.1.0
 * @packege CMS
 * @license GNU General Public License, version 3
 */
class Milano_Test_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * getConfigData
     *
     * Returns the value for the requested configuration.
     * @param string $data
     * @return mixed
     */
    public function getConfigData($data)
    {
        return Mage::getStoreConfig('milano_test/' . $data);
    }

    /**
     * isEnabled
     *
     * Returns true if the module is enabled to be displayed.
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->getConfigData('configuration/enabled');
    }

}
